
import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.image.*;
import javax.swing.JFrame;
import java.util.Random;
import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;

/**
 *
 * @author agpeil
 */
public class AnimacaoBocha extends JFrame {

    SimpleUniverse us;
    Canvas3D meuCanvas3D;

    /**
     *
     */
    public AnimacaoBocha() {
        init();
    }

    public void init() {

        //Mechanism for closing the window and ending the program.
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Default settings for the viewer parameters.
        meuCanvas3D = new Canvas3D(SimpleUniverse.getPreferredConfiguration());

        //The scene is generated in this method.
        BranchGroup cena = criaCena();
        //Construct the SimpleUniverse:
        //First generate it using the Canvas.
        us = new SimpleUniverse(meuCanvas3D);

        //Default position of the viewer.
        us.getViewingPlatform().setNominalViewingTransform();


        //The following three lines enable navigation through the scene using the mouse.
        OrbitBehavior ob = new OrbitBehavior(meuCanvas3D, OrbitBehavior.REVERSE_ALL);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
        ob.setSchedulingBounds(bounds);
        us.getViewingPlatform().setViewPlatformBehavior(ob);

        us.addBranchGraph(cena);

        //Show the canvas/window.
        setTitle("Trabalho 3 - Cena 3D");
        setSize(1024, 768);
        getContentPane().add("Center", meuCanvas3D);
        setVisible(true);


    }

    public BranchGroup criaCena() {
        String timeA = "textura1.jpg";
        String timeA2 = "textura2.jpg";
        String bolin = "texturaBolin.jpg";
        String timeB = "textura3.jpg";
        String timeB2 = "textura4.jpg";
        BranchGroup aCena = new BranchGroup();
        Appearance textura = new Appearance();
        Random gerador = new Random();
        float x, y, z;

        aCena.addChild(addMovLuz());


        // CRIANDO O BOLIN
        TransformGroup tgBolin = new TransformGroup();
        textura = addTextura(us, bolin);
        x = 0.25f;
        y = 2.1f;
        z = -15.8f;
        tgBolin = addBocha(us, textura, x, y, z, true);
        aCena.addChild(tgBolin);


        // CRIANDO TIME VERMELHO
        // =======================================
        TransformGroup tgBolaVerm = new TransformGroup();
        textura = addTextura(us, timeA);
        x = 1.32f;
        y = 3.6f;
        z = -22.5f;
        tgBolaVerm = addBocha(us, textura, x, y, z, false);
        aCena.addChild(tgBolaVerm);

        textura = addTextura(us, timeA);
        textura.setTransparencyAttributes(addTransparencia());
        x = -0.920f;
        y = 1.9f;
        z = -15.9f;
        tgBolaVerm = addBocha(us, textura, x, y, z, false);
        aCena.addChild(tgBolaVerm);

        textura = addTextura(us, timeA2);
        x = 1.016f;
        y = 2.6f;
        z = -19.7f;
        tgBolaVerm = addBocha(us, textura, x, y, z, false);
        aCena.addChild(tgBolaVerm);

        // diferença pra uma e outra não colar 4.75 

        // CRIANDO TIME AZUL
        // =======================================
        TransformGroup tgBolaAzul = new TransformGroup();
        textura = addTextura(us, timeB);

        x = 1.2f;
        y = 2.2f;
        z = -18.3f;
        tgBolaAzul = addBocha(us, textura, x, y, z, false);
        aCena.addChild(tgBolaAzul);

        textura = addTextura(us, timeB2);
        x = -1.0f;
        y = 2.5f;
        z = -20.8f;
        tgBolaAzul = addBocha(us, textura, x, y, z, false);
        aCena.addChild(tgBolaAzul);


        textura = addTextura(us, timeB2);
        textura.setTransparencyAttributes(addTransparencia());
        x = 1.15f;
        y = 3.4f;
        z = -21.2f;
        tgBolaAzul = addBocha(us, textura, x, y, z, false);
        aCena.addChild(tgBolaAzul);




        //  CRIA BOCHA VERMELHA QUE SERA MOVIMENTADA
        // ================================================

        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), Double.MAX_VALUE);
        textura = addTextura(us, timeA2);
        x = 0.0f;
        y = 0.0f;
        z = -18.0f;
        Sphere bochaVerm = new Sphere(0.19f, Sphere.GENERATE_NORMALS, 100, textura);

        TransformGroup tgBochaVerm = new TransformGroup();
        tgBochaVerm.addChild(bochaVerm);

        // =========

        // CRIA MOVIMENTO DE ROTACAO DA BOCHA VERMELHA
        //======================================================


        Transform3D tRotacao = new Transform3D();

        Alpha alphaRotation = new Alpha(5,
                Alpha.INCREASING_ENABLE, 1, 0, 1800, 0, 0, 0, 0, 0);

        RotationInterpolator iRotacao = new RotationInterpolator(
                alphaRotation, tgBochaVerm,
                tRotacao, 0.0f, (float) Math.PI * 2);

        iRotacao.setSchedulingBounds(bounds);
        TransformGroup tgRotacao = new TransformGroup();
        tgRotacao.addChild(iRotacao);

        tgBochaVerm.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        tgBochaVerm.addChild(tgRotacao);


        Transform3D tfBocha = new Transform3D();
        tfBocha.rotX(Math.PI * 2 / 8);              // rotacionando os eixos
        tfBocha.rotY(Math.PI * 2 / 8);              // rotacionando os eixos
        tfBocha.rotZ(Math.PI * 2 / 7.8);            // rotacionando os eixos

        tfBocha.setTranslation(new Vector3f(0.0f, 0.0f, 0.0f));
        TransformGroup tgBocha = new TransformGroup(tfBocha);
        tgBocha.addChild(tgBochaVerm);
        //tgBocha.addChild();
        // =========

        // CRIA O MOVIMENTO DA BOCHA VERMELHA
        //==============================================


        TransformGroup tgMov = new TransformGroup();
        tgMov.addChild(tgBocha);

        Transform3D bochaMov = new Transform3D();
        bochaMov.rotZ(Math.PI * 2 / 7.8);           // rotacionando os eixos
        bochaMov.setTranslation(new Vector3f(0.0f, 1.0f, -15.7f));


        Alpha teste = new Alpha(1, Alpha.INCREASING_ENABLE
                + Alpha.DECREASING_ENABLE, 0, 0, 4000, 0, 0, 6000, 0, 0);


        PositionInterpolator posIMov1 = new PositionInterpolator(
                teste, tgMov, bochaMov, 1f, -7.7f);


        posIMov1.setSchedulingBounds(bounds);

        tgMov.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        tgMov.addChild(posIMov1);

        TransformGroup tgMovCompleto = new TransformGroup(bochaMov);
        tgMovCompleto.addChild(tgMov);
        tgMovCompleto.addChild(addLuzDirecional());
        // =========


        //  CRIA BOCHA AZUL QUE SERA MOVIMENTADA
        // ================================================

        textura = addTextura(us, timeB2);
        x = 0.05f;
        y = 2.1f;
        z = -1.2f;
        Sphere bochaAzul = new Sphere(0.18f, Sphere.GENERATE_NORMALS, 100, textura);

        TransformGroup tgBochaAzul = new TransformGroup();
        tgBochaAzul.addChild(bochaAzul);

        // =========


        // CRIA O MOVIMENTO DA BOCHA AZUL
        //==============================================

        TransformGroup tgMov2 = new TransformGroup();
        tgMov2.addChild(tgBochaAzul);

        Transform3D bochaMov2 = new Transform3D();

        bochaMov2.rotZ(Math.PI * 2 / 8);           // rotacionando os eixos
        bochaMov2.setTranslation(new Vector3f(0.0f, 0.0f, 0.0f));

        Alpha teste2 = new Alpha(1, Alpha.INCREASING_ENABLE, 10000, 0, 1000, 0, 0, 0, 0, 0);

        PositionInterpolator posIMov2 = new PositionInterpolator(
                teste2, tgMov2, bochaMov2, 1f, 1.5f);

        posIMov2.setSchedulingBounds(bounds);

        tgMov2.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        tgMov2.addChild(posIMov2);

        TransformGroup tgMovCompleto2 = new TransformGroup(bochaMov);
        tgMovCompleto2.addChild(tgMov2);

        aCena.addChild(addBackground());

        aCena.addChild(tgMovCompleto);
        aCena.addChild(tgMovCompleto2);

        return aCena;
    }

    /**
     *
     * @param us
     * @param textura
     * @param x
     * @param y
     * @param z
     * @param flag
     * @return
     */
    public TransformGroup addBocha(SimpleUniverse us, Appearance textura, float x, float y, float z, boolean flag) {
        Sphere bocha;
        if (flag) {
            bocha = new Sphere(0.1f, Sphere.GENERATE_NORMALS, 100, textura);
        } else {
            bocha = new Sphere(0.2f, Sphere.GENERATE_NORMALS, 100, textura);
        }

        Transform3D tBocha = new Transform3D();
        tBocha.setTranslation(new Vector3f(x, y, z));
        TransformGroup tgBocha = new TransformGroup(tBocha);
        tgBocha.addChild(bocha);

        return tgBocha;

    }

    public BranchGroup addLuzDirecional() {
        BoundingSphere bounds = new BoundingSphere(new Point3d(6.0, 9.0, 10.0), Double.MAX_VALUE);
        BranchGroup bgLight = new BranchGroup();
        DirectionalLight dir1 = new DirectionalLight();
        dir1.setEnable(true);
        dir1.setColor(new Color3f(1.0f, 1.0f, 1.0f));
        dir1.setDirection(new Vector3f(0.8f, -0.35f, 0.5f));
        dir1.setInfluencingBounds(bounds);
        bgLight.addChild(dir1);

        return bgLight;
    }

    public BranchGroup addMovLuz() {

        BranchGroup bgLight = new BranchGroup();
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), Double.MAX_VALUE);
        Color3f lightColour1 = new Color3f(11.5f, 11.5f, 11.9f);
        Vector3f lightDir1 = new Vector3f(0.0f, -0.9f, -1.0f);
        DirectionalLight light = new DirectionalLight(lightColour1, lightDir1);
        light.setInfluencingBounds(bounds);
       TransformGroup tfmLight = new TransformGroup();
        tfmLight.addChild(light);
        Alpha alphaLight = new Alpha(-1, 8000);
        RotationInterpolator rot = new RotationInterpolator(alphaLight, tfmLight, new Transform3D(), 0.0f, (float) Math.PI * 2);
        rot.setSchedulingBounds(bounds);
        tfmLight.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        tfmLight.addChild(rot);
        bgLight.addChild(tfmLight);

        return bgLight;

    }

    /**
     *
     * @param us
     * @param imagem
     * @return
     */
    public Appearance addTextura(SimpleUniverse us, String imagem) {
        TextureLoader textureLoad = new TextureLoader(imagem, null);
        ImageComponent2D textureIm = textureLoad.getScaledImage(1024, 768);
        Texture2D aTexture = new Texture2D(Texture2D.BASE_LEVEL, Texture2D.RGB,
                textureIm.getWidth(),
                textureIm.getHeight());
        aTexture.setImage(0, textureIm);

        Appearance textureApp = new Appearance();
        textureApp.setTexture(aTexture);
        TextureAttributes textureAttr = new TextureAttributes();
        textureAttr.setTextureMode(TextureAttributes.REPLACE);
        textureApp.setTextureAttributes(textureAttr);
        Material mat = new Material();
        mat.setShininess(120.0f);
        textureApp.setMaterial(mat);
        TexCoordGeneration tcg = new TexCoordGeneration(TexCoordGeneration.OBJECT_LINEAR,
                TexCoordGeneration.TEXTURE_COORDINATE_2);

        textureApp.setTexCoordGeneration(tcg);


        return textureApp;
    }

    public TransparencyAttributes addTransparencia() {
        TransparencyAttributes ta = new TransparencyAttributes();
        ta.setTransparencyMode(TransparencyAttributes.BLENDED);
        ta.setTransparency(0.5f);
        
        return ta;
    }

    public Background addBackground() {
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), Double.MAX_VALUE);
        TextureLoader textureLoad = new TextureLoader("background2.jpg", null);
        Background bgImage = new Background(textureLoad.getImage());
        bgImage.setApplicationBounds(bounds);

        return bgImage;
    }

    /**
     * Generates a default surface (Appearance) in a specified colour.
     *
     * @param app The Appearance for the surface.
     * @param col The colour.
     */
    public static void setToMyDefaultAppearance(Appearance app, Color3f col) {
        app.setMaterial(new Material(col, col, col, col, 120.0f));
    }
}
